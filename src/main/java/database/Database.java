/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;
// Singletion pattern

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class Database {

    private static Database instance = new Database();
    private Connection conn;

    private Database() {
    }

    public static Database getInstance() {

        String dbPath = "./db/Store.db";


        try {
            //create a connection
            if (instance.conn == null || instance.conn.isClosed()) {
                Class.forName("org.sqlite.JDBC");
                instance.conn = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
                System.out.println("Databest Connection");
            }
        } catch (ClassNotFoundException xe) {
            System.out.println("Eror: JDBC is not exist");
        } catch (SQLException ex) {
            System.out.println("Eror: Database cannot connection");
        }
        return instance;
    }

    public static void close() {
        try {
            if (instance.conn != null && !instance.conn.isClosed()) {
                instance.conn.close();
                
            }
        } catch (SQLException ex) {
            System.out.println("Error: yes is error.");
        }
        instance.conn = null;
    }
    
    public Connection getConnection(){
        return instance.conn;
    }
}
